# Mining.Libre Smart Contract for Perpetual DAO Funding

This contract oversees BTC contributions, and the dispensation of (staked) LIBRE coins throughout a 10-year timeframe, incorporating a halving mechanism for the amount of LIBRE emitted daily.

At six-month intervals, this smart contract will undergo halving, starting from an initial distribution rate of 2,000,000.0000 LIBRE coins daily, with successive halving events occurring every 6 months until the final phase, where only 10 LIBRE coins are dispensed per day.

Key functionalities encompass the monitoring of BTC contributions, the allocation of LIBRE coins pro-rata daily based on each contribution as a % of the total contributions, the establishment of LIBRE stakes, stake claiming, and subsequent LIBRE token distribution to contributors following a mandated 6-month lockup period.

This smart contract operates as an arbiter of equitable and transparent LIBRE token distribution, affording users the facility to verify and redeem their stakes. It acts as a pivotal tool for incentivizing engagement and contributions within the Libre blockchain ecosystem through continuous DAO funding.

## For technical instructions, see the following section

- [How to get started?](./contracts/mining/README.md)

## Dependencies

- Provision "stake" permission access of the eosio account to the mining.libre contract, enabling the issuance of daily coins in accordance with the distribution configuration.
- Ensure the existence of the stake.libre contract.
- Establish a cron job to run the process action as scheduled.

## Specifications

- Understand the 6-month duration as comprising 30 days per month, totaling 180 days.
- Contributions will be open for acceptance for a duration of 10 years (3650 days) from the initialization point.
- Facilitate BTC contributions exclusively through the btc.libre or Libre Native BTC Bridge contract; any transactions featuring a distinct token symbol or originating from a different contract will be rejected.
- Distribute LIBRE coins based on daily contributions, conducted at the conclusion of each 24-hour interval.
- Generate fresh 6 month stakes utilizing the stake.libre contract upon the culmination of each time slot.
- Direct any surplus from the allocation process to the dao.libre contract, regardless of whether the remaining amount is incomplete or if no contributions were made.
- Tokens transferred to the mining.libre contract outside of the designated and authorized allocation schedule will not be factored into the allocation process; instead, they will be directed to the dao.libre contract at the commencement of each halving phase.
- Transfer the contributed BTC to the dao.libre contract within each 24-hour slot, delineated as commencing from 00:00:00 and ending at 23:59:59.500.
- Following the 6-month lockup period from the initiation of contributions to the Smart Contract:
  - Process any available stakes every 24 hours.
  - Withdraw LIBRE coins claimed by contributors every 24 hours.

## Example

There are two accounts, namely alice and bob.

- On day 1, both alice and bob contribute 0.01 BTC each, which is recorded in the contribution table.
- At the day's conclusion, the process is executed, tallying all BTC contributions, and LIBRE coins are allocated in the stakes table proportional to each individual contribution.
- At the close of each day, the BTC contributed is forwarded to dao.libre.
- Subsequently, each contributor receives 1 million LIBRE coins, as allocated in the stakes table.
- These tokens are automatically staked for 6 months in the stake.libre contract by the mining.libre contract within the process action.
- The tokens are staked for a duration of 6 months, and are controlled by the mining contract.
- The mining.libre contract is capable of executing a claim action to ascertain if there are any available claims on stake.libre for the stakes it has created.
- If a stake is claimed, it updates the contributions table and can be distributed back to the corresponding account that made the original contribution.

## Key Structure

The mining contract permission is made of:
| Parent permission | Permission | Authority |
|-------------------|------------|---------------------|
| | Owner | mining.libre@eosio.code <br> eosio.prods@active |
| Owner | Active | eosio.prods@active <br> mining.libre@eosio.code|

## Actions

The subsequent enumeration delineates the contract interactions available to users:

### on_transfer (notify)

This function serves as an event listener and, while not an action, it acts as the primary trigger to initiate a contribution. It monitors transfer events originating from the allowed token contracts with the right BTC token symbol.

#### Parameters

- from: BTC contributor.
- to: mining.libre.
- quantity: BTC contribution amount.
- memo: optional field to specify the purpose of the transfer.

### init

Specify the starting time for accepting contributions.

#### Parameters

- start_date: Time for when the contract will start accepting contributions.

### process

Track BTC contributions, issues the daily LIBRE, and distributes that LIBRE according to the designated distribution quantity.

#### Parameters

- limit: int - Maximum number of table rows to process in a single execution.

### withdraw

Distribute LIBRE coins to contributors.

#### Parameters

- id: Unique identifier of the contribution made.

## Tables

The essential tables necessary to fulfill the contract's requirements are listed below:

### global (singleton)

Monitor global parameters to establish the initial activation date for enabling contract actions and additional functionalities.

Some of the initial attributes for this table are: start_date, end_date, current_allocation_per_day, total_amount_to_allocate, total_amount_allocated and next_halving_date.

### contribution

Store contributions made during the 24-hour slot period and allocate the corresponding amount of LIBRE rewards for each contribution at the end of the 6-month lockup period.

Some of the initial attributes for this table are: index, account, btc_amount, contr_date, stake_index, stake_amount, stake_payout, stake_date and status.

## Contribution Status Numbers

The mining contract uses the following status numbers to track the state of each contribution:

0: PENDING
1: PENDING_LINK_STAKE
2: STAKE_LINKED
3: CLAIMED
4: WITHDRAWN
5: FROZEN

These status numbers are used in the `contribution` table to indicate the current state of each contribution.

#### Secondary Indexes

- byaccount: filter by contributor.
- bycontrdate: filter by date the contribution was made.
- bystatus: filter by pending, ready_to_claim and claimed.

### stakeprocess (singleton)

Manage calculation processes and operations to determine the daily LIBRE allocation per account.

Some of the initial attributes for this table are: 0 and 1.

Note: this table has the std::variant structure, this means, their attributes may change depending on the status of the contract, whether it is in standby, count, allocate, link or claim.

### distribution

Store the total reward amount per account.

Some of the initial attributes for this table are: account, reward_amount and total_claimed.
These are the initial table attributes but they may change as needed during the development phase.
Level of effort
The following table is an evaluation of the level of effort required to implement the new contract.

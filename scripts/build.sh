#!/usr/bin/env bash
set -e

echo "Host compilation is disabled"
exit 1

echo "Compiling contracts..."

BUILD_TESTS=OFF

mkdir -p build
cd build && cmake -DBUILD_TESTS=${BUILD_TESTS} .. && make -j $(nproc) && cd ..

if [ $? -eq 0 ]; then
  echo "Contracts compiled successfully"
  echo "Copying contracts to contracts folder..."
  cp ./build/contracts/mining/mining.* ./contracts/mining
  echo "Files copied successfully"
else
  echo "Compilation failed"
fi

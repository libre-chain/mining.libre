#!/usr/bin/env sh
set -e

rm -rf ./build-docker

docker build -t contract-builder:latest -f docker/builder.Dockerfile .
docker run --name contract-builder contract-builder:latest
docker cp contract-builder:/build ./build-docker
docker rm contract-builder

pwd

echo "Copying files from docker container"
cp build-docker/mining.* ./contracts/mining/  || true
echo "WASM hash: $(sha256sum contracts/mining/mining.wasm)"
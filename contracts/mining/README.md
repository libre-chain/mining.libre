# BTC Bridge Project

## Dependencies

- [CLSDK](https://github.com/gofractally/contract-lab)
- Docker (if using docker to build)

## Set the right build configuration

There is a `CMakeLists.txt` file in the root of the mining folder. You can set the build configuration by changing the `set(BUILD_TESTNET OFF)` line to either `OFF` or `ON` and `set(RUN_TESTS OFF)` line to either `OFF` or `ON`.

The result of twicking these values will be:

- `set(BUILD_TESTNET OFF)` and `set(RUN_TESTS OFF)`: Build for production and do not run tests.
- `set(BUILD_TESTNET ON)` and `set(RUN_TESTS OFF)`: Build for Testnet and do not run tests.
- `set(BUILD_TESTNET OFF)` and `set(RUN_TESTS ON)`: Build for production and run tests.
- `set(BUILD_TESTNET ON)` and `set(RUN_TESTS ON)`: Build for Testnet and run tests.

Test are only configured for production environment, so the `ON`, `ON` configuration will not work as expected.

**IMPORTANT NOTE**

When building for Mainnet, disable the tests, as they remove some functionalities of the smart contract because of limitations of the testing framework. So the configuration is:

Mainnet

`set(BUILD_TESTNET OFF)` and `set(RUN_TESTS OFF)`: Build for production and do not run tests.

or

Testnet

`set(BUILD_TESTNET ON)` and `set(RUN_TESTS OFF)`: Build for Testnet and do not run tests.

If you want a more custom configuration, you can go to `./mininig.libre/include/_config.hpp.in` and update the values based on your needs.

## How to Build with docker

Go to root of repository and run this:

`./scripts/docker-build.sh`

## How to initialize the contract?

To get the smart contract ready for contributions, it requires to be initialized, to do so, the active permission of the contract account is needed with the following values depending on the target network.

### Testnet

```bash
cleos -u <node_endpoint> push action mining.libre init '["2000.0000 LIBRE", "2024-05-01T00:00:00.000", 30]' -p mining.libre@active
```

### Mainnet

```bash
cleos -u <node_endpoint> push action mining.libre init '["2000000.0000 LIBRE", "2024-05-01T00:00:00.000", 3600]' -p mining.libre@active
```

The are 3 values that are passed to the `init` action:

`[0]`: Initial supply of the token. For Mainnet it must be `2,000,000.0000 LIBRE` and for Testnet it must be `2,000.0000` LIBRE.

`[1]`: Start date for the mining period.

`[2]`: Total days the contract will be active.

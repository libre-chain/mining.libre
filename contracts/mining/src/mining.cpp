#include <mining.hpp>

namespace mining {
  void mining::notify_transfer( eosio::name  &from,
                                eosio::name  &to,
                                eosio::asset &quantity,
                                std::string  &memo ) {
    // tokens from the contract
    if ( from == get_self() ) {
      sub_balance( to, quantity );

      return;
    }

    // skip transactions that are not related
    if ( to != get_self() ) {
      return;
    }

    if ( SUPPORTED_TOKEN_LIBRE_CONTRACT == get_first_receiver() &&
         quantity.symbol == SUPPORTED_TOKEN_LIBRE_SYMBOL ) {
      // set_global( quantity );

      return;
    }

    if ( SUPPORTED_TOKEN_BTC_CONTRACT == get_first_receiver() ) {
      eosio::check( quantity.symbol == SUPPORTED_TOKEN_BTC_SYMBOL,
                    "invalid symbol, expected: " +
                        SUPPORTED_TOKEN_BTC_SYMBOL.code().to_string() );

      add_contribution( from, quantity );

      return;
    }

    eosio::check( false, "invalid token contract" );
  }

  void mining::init( eosio::asset          &initial_libre_allocation,
                     eosio::time_point_sec &start_date,
                     uint16_t               total_days ) {
    require_auth( get_self() );

    // symbol and amount are compared under the hood
    eosio::check( initial_libre_allocation == INITIAL_LIBRE_ALLOCATION,
                  "expected initial allocation to be " +
                      INITIAL_LIBRE_ALLOCATION.to_string() );

    if ( global_sing.exists() ) {
      return;
    }

    auto global = global_sing.get_or_default();

    eosio::check( total_days == CONTRACT_ACTIVE_TOTAL_DAYS,
                  "expected total days: " +
                      std::to_string( CONTRACT_ACTIVE_TOTAL_DAYS ) );
    eosio::check( start_date >=
                      eosio::time_point_sec( eosio::current_time_point() ),
                  "start date must be in the future" );

    global.initial_libre_allocation = initial_libre_allocation;
    global.total_amount_allocated =
        eosio::asset( 0, SUPPORTED_TOKEN_LIBRE_SYMBOL );
    global.start_date = start_date;
    global.end_date =
        start_date + eosio::days( static_cast< int64_t >( total_days ) );
    global.current_allocation_per_day = initial_libre_allocation;
    global.next_halving_date =
        eosio::time_point_sec( start_date + eosio::days( HALVING_STEP ) );

    global_sing.set( global, get_self() );
    stakeprocess_sing.get_or_create(
        get_self(),
        stakeprocess_standby{ .next_contribution_date =
                                  start_date +
                                  eosio::hours( CONTRIBUTION_TIME_HOUR_SLOT ),
                              .next_stake_index = START_STAKE_INDEX_LOOKUP,
                              .next_index_cache = 0 } );
  }

  void mining::process( uint16_t limit ) {
    uint16_t              limit_passed = limit;
    eosio::time_point_sec current_time_sec =
        eosio::time_point_sec( eosio::current_time_point().sec_since_epoch() );

    auto global = global_sing.get_or_default();

    eosio::check( global.end_date != eosio::time_point_sec(),
                  "contract not initialized" );

    auto stakeprocess = stakeprocess_sing.get_or_default();

    if ( auto *process =
             std::get_if< stakeprocess_standby >( &stakeprocess ) ) {

      if ( process->next_contribution_date > global.next_halving_date &&
           global.end_date > process->next_contribution_date ) {
        apply_halving();
      }

      if ( global.end_date < process->next_contribution_date ) {
        stakeprocess = stakeprocess_claim{
            .contribution_date = process->next_contribution_date,
            .next_stake_index = process->next_stake_index,
            .next_index_cache = process->next_index_cache };
      } else {
        eosio::check( current_time_sec >= process->next_contribution_date,
                      "not ready to process" );

        stakeprocess = stakeprocess_count{
            .contribution_date = process->next_contribution_date,
            .next_index = process->next_index_cache,
            .total_pbtc_contributed =
                eosio::asset( 0, SUPPORTED_TOKEN_BTC_SYMBOL ),
            .next_stake_index = process->next_stake_index,
            .next_index_cache = process->next_index_cache };

#ifndef RUN_TESTS
        // this is for mainnet only
        global = global_sing.get_or_default();
        issue_and_transfer_tokens( global.current_allocation_per_day );
#endif
      }

      stakeprocess_sing.set( stakeprocess, get_self() );
    }

    stakeprocess = stakeprocess_sing.get();

    if ( auto *process = std::get_if< stakeprocess_count >( &stakeprocess ) ) {
      auto copy = *process;
      process_count( limit_passed, copy );
    }

    stakeprocess = stakeprocess_sing.get();

    if ( auto *process =
             std::get_if< stakeprocess_allocate >( &stakeprocess ) ) {
      auto copy = *process;
      process_allocate( limit_passed, copy );

      // return is made on purpose because it is required the stakes are proccessed by
      // the stake.libre contract
      return;
    }

    stakeprocess = stakeprocess_sing.get();

    if ( auto *process = std::get_if< stakeprocess_link >( &stakeprocess ) ) {
      auto copy = *process;
      process_link( limit_passed, copy );
    }

    stakeprocess = stakeprocess_sing.get();

    if ( auto *process = std::get_if< stakeprocess_claim >( &stakeprocess ) ) {
      eosio::time_point_sec ctp_sec = current_time_sec;

      eosio::time_point_sec start_claiming_time =
          global.start_date + eosio::days( DEFAULT_TOTAL_STAKE_DAYS );

      if ( ctp_sec > start_claiming_time ) {
        auto copy = *process;
        process_claim( limit_passed, copy );
      } else {
        auto sp = stakeprocess_sing.get();

        sp = stakeprocess_standby{
            .next_contribution_date =
                process->contribution_date +
                eosio::hours( CONTRIBUTION_TIME_HOUR_SLOT ),
            .next_stake_index = process->next_stake_index,
            .next_index_cache = process->next_index_cache };

        --limit_passed;

        stakeprocess_sing.set( sp, get_self() );
      }
    }

#ifndef BUILD_TESTNET
    // this is for mainnet only.
    // contract was developed considering that stakes are going to be available to claim after 180 days
    // from the start contribution date, so, this way, the scenario where a contribution date is lower than the
    // current contribution time shouldn't exist unless end contribution period > start contribution period + DEFAULT_TOTAL_STAKE_DAYS.
    eosio::check( limit_passed != limit, "nothing to process" );
#endif
  }

  void mining::withdraw( uint64_t index ) {
    auto curr_contr = contribution_tb.find( index );

    eosio::check( curr_contr != contribution_tb.end(),
                  "contribution not found" );
    eosio::check( curr_contr->status == e_stake_status::CLAIMED,
                  "contribution is not ready to be claimed" );

    require_auth( curr_contr->account );

    add_balance( curr_contr->account, curr_contr->stake_payout );

    std::string memo = "withdraw funds from: " + get_self().to_string();

    eosio::action( eosio::permission_level{ get_self(), "active"_n },
                   SUPPORTED_TOKEN_LIBRE_CONTRACT,
                   "transfer"_n,
                   std::make_tuple( get_self(),
                                    curr_contr->account,
                                    curr_contr->stake_payout,
                                    memo ) )
        .send();

    contribution_tb.modify( curr_contr, get_self(), [&]( auto &row ) {
      row.status = e_stake_status::WITHDRAWN;
    } );
  }

  // private methods
  void mining::add_contribution( eosio::name  &account,
                                 eosio::asset &quantity ) {
    auto global = global_sing.get_or_default();

    eosio::check( global.end_date != eosio::time_point_sec(),
                  "contract not initialized" );
    eosio::check( eosio::time_point_sec( eosio::current_time_point() ) >=
                      global.start_date,
                  "contribution period not open yet" );
    eosio::check( eosio::time_point_sec( eosio::current_time_point() ) <
                      global.end_date,
                  "contribution period has ended" );

    contribution_tb.emplace( get_self(), [&]( auto &row ) {
      row.index = contribution_tb.available_primary_key();
      row.account = account;
      row.btc_amount = quantity;
      row.contr_date = eosio::current_time_point();
      row.stake_amount = eosio::asset( 0, SUPPORTED_TOKEN_LIBRE_SYMBOL );
      row.stake_payout = eosio::asset( 0, SUPPORTED_TOKEN_LIBRE_SYMBOL );
      row.stake_date = eosio::time_point_sec();
      row.status = e_stake_status::PENDING;
    } );
  }

  void mining::process_count( uint16_t           &limit,
                              stakeprocess_count &stakeprocess ) {
    if ( limit == 0 ) {
      return;
    }

    auto start_slot_time =
        eosio::time_point_sec( stakeprocess.contribution_date -
                               eosio::hours( CONTRIBUTION_TIME_HOUR_SLOT ) );
    auto curr_contr = contribution_tb.find( stakeprocess.next_index );
    auto end_contr = contribution_tb.end();

    for ( ;
          curr_contr != end_contr &&
          curr_contr->contr_date < stakeprocess.contribution_date && limit > 0;
          ++curr_contr, --limit ) {
      if ( curr_contr->status == e_stake_status::PENDING &&
           curr_contr->contr_date >= start_slot_time ) {
        stakeprocess.total_pbtc_contributed += curr_contr->btc_amount;
      }
    }

    auto process = stakeprocess_sing.get();

    if ( curr_contr != end_contr &&
         curr_contr->contr_date < stakeprocess.contribution_date ) {
      stakeprocess.next_index = curr_contr->index;
      process = stakeprocess;
    } else {
      auto global = global_sing.get();

      process = stakeprocess_allocate{
          .contribution_date = stakeprocess.contribution_date,
          .next_index = stakeprocess.next_index_cache,
          .total_pbtc_contributed = stakeprocess.total_pbtc_contributed,
          .amount_allocated = eosio::asset( 0, SUPPORTED_TOKEN_LIBRE_SYMBOL ),
          .current_allocation = global.current_allocation_per_day,
          .next_stake_index = stakeprocess.next_stake_index,
          .next_index_cache = stakeprocess.next_index_cache };
    }

    stakeprocess_sing.set( process, get_self() );
  }

  void mining::process_allocate( uint16_t              &limit,
                                 stakeprocess_allocate &stakeprocess ) {
    if ( limit == 0 ) {
      return;
    }

    auto start_slot_time =
        eosio::time_point_sec( stakeprocess.contribution_date -
                               eosio::hours( CONTRIBUTION_TIME_HOUR_SLOT ) );
    auto global = global_sing.get_or_default();
    auto curr_contr = contribution_tb.find( stakeprocess.next_index );
    auto end_contr = contribution_tb.end();

    for ( ;
          curr_contr != end_contr &&
          curr_contr->contr_date < stakeprocess.contribution_date && limit > 0;
          ++curr_contr, --limit ) {
      if ( curr_contr->status != e_stake_status::PENDING ||
           curr_contr->contr_date < start_slot_time ) {
        continue;
      }

      auto stake_amount =
          calculate_allocation_reward( curr_contr->btc_amount,
                                       stakeprocess.total_pbtc_contributed,
                                       stakeprocess.current_allocation );

      if ( stake_amount.amount > 0 ) {
        add_balance( SUPPORTED_STAKING_CONTRACT, stake_amount );

        std::string memo =
            "stakefor:" + std::to_string( DEFAULT_TOTAL_STAKE_DAYS );

        eosio::action( eosio::permission_level{ get_self(), "active"_n },
                       SUPPORTED_TOKEN_LIBRE_CONTRACT,
                       "transfer"_n,
                       std::make_tuple( get_self(),
                                        SUPPORTED_STAKING_CONTRACT,
                                        stake_amount,
                                        memo ) )
            .send();
      }

      contribution_tb.modify( curr_contr, get_self(), [&]( auto &row ) {
        row.stake_amount = stake_amount;
        row.status = stake_amount.amount > 0
                         ? e_stake_status::PENDING_LINK_STAKE
                         : e_stake_status::FROZEN;
      } );

      global.total_amount_allocated += stake_amount;
      stakeprocess.amount_allocated += stake_amount;
    }

    global_sing.set( global, get_self() );

    auto process = stakeprocess_sing.get();

    if ( curr_contr != end_contr &&
         curr_contr->contr_date < stakeprocess.contribution_date ) {
      stakeprocess.next_index = curr_contr->index;
      process = stakeprocess;
    } else {
      if ( stakeprocess.current_allocation.amount >
           stakeprocess.amount_allocated.amount ) {
        send_leftofver_allocation( stakeprocess.current_allocation -
                                       stakeprocess.amount_allocated,
                                   stakeprocess.contribution_date );

        stakeprocess.amount_allocated.amount = 0;
      }

      if ( stakeprocess.total_pbtc_contributed.amount > 0 ) {
        send_daily_pbtc_contribution( stakeprocess.total_pbtc_contributed,
                                      stakeprocess.contribution_date );
      }

      process = stakeprocess_link{
          .next_index = stakeprocess.next_index_cache,
          .next_stake_index = stakeprocess.next_stake_index,
          .contribution_date = stakeprocess.contribution_date,
          .next_index_cache = stakeprocess.next_index_cache,
          .new_contrs = stakeprocess.total_pbtc_contributed.amount > 0 };
    }

    stakeprocess_sing.set( process, get_self() );
  }

  void mining::process_link( uint16_t          &limit,
                             stakeprocess_link &stakeprocess ) {
    if ( limit == 0 ) {
      return;
    }

    auto start_slot_time =
        eosio::time_point_sec( stakeprocess.contribution_date -
                               eosio::hours( CONTRIBUTION_TIME_HOUR_SLOT ) );

    libre::stake_table stake_tb( SUPPORTED_STAKING_CONTRACT,
                                 SUPPORTED_STAKING_CONTRACT.value );
    auto stake_itr = stake_tb.lower_bound( stakeprocess.next_stake_index );
    auto stake_itr_end = stake_tb.end();
    auto curr_contr = contribution_tb.find( stakeprocess.next_index );
    auto contr_end = contribution_tb.end();

    for ( ;
          curr_contr != contr_end &&
          curr_contr->contr_date < stakeprocess.contribution_date && limit > 0;
          ++curr_contr, --limit ) {
      if ( curr_contr->status != e_stake_status::PENDING_LINK_STAKE ||
           curr_contr->contr_date < start_slot_time ) {
        continue;
      }

      while ( stake_itr != stake_itr_end && limit > 0 ) {
        if ( stake_itr->index >= stakeprocess.next_stake_index &&
             stake_itr->account == get_self() &&
             stake_itr->libre_staked == curr_contr->stake_amount &&
             stake_itr->stake_date >= curr_contr->contr_date ) {
          break;
        }

        --limit;
        ++stake_itr;
      }

      if ( limit == 0 ) {
        break;
      }

      if ( stake_itr == stake_itr_end ) {
        eosio::check( false, "stake corrupted: not found" );
      }

      stakeprocess.next_stake_index = stake_itr->index;

      contribution_tb.modify( curr_contr, get_self(), [&]( auto &row ) {
        row.stake_index = stake_itr->index;
        row.stake_date = stake_itr->stake_date;
        row.stake_payout = stake_itr->payout;
        row.status = e_stake_status::STAKE_LINKED;
      } );

      stakeprocess.next_index = curr_contr->index;
      ++stake_itr;
    }

    auto process = stakeprocess_sing.get();

    if ( curr_contr != contr_end &&
         curr_contr->contr_date < stakeprocess.contribution_date ) {
      stakeprocess.next_index = curr_contr->index;
      stakeprocess.next_stake_index = stake_itr->index;
      process = stakeprocess;
    } else {
      process = stakeprocess_claim{
          .contribution_date = stakeprocess.contribution_date,
          .next_stake_index = stakeprocess.next_stake_index +
                              ( stakeprocess.new_contrs ? 1 : 0 ),
          .next_index_cache =
              stakeprocess.next_index + ( stakeprocess.new_contrs ? 1 : 0 ) };
    }

    stakeprocess_sing.set( process, get_self() );
  }

  void mining::process_claim( uint16_t           &limit,
                              stakeprocess_claim &stakeprocess ) {
    if ( limit == 0 ) {
      return;
    }

    auto ctp = eosio::current_time_point();
    auto end_slot_time =
        eosio::time_point_sec( ctp - eosio::days( DEFAULT_TOTAL_STAKE_DAYS ) );
    auto contr_bystatus_index = contribution_tb.get_index< "bystatus"_n >();
    auto indexby_value =
        static_cast< uint64_t >( e_stake_status::STAKE_LINKED );
    auto curr_contr = contr_bystatus_index.lower_bound( indexby_value );
    auto end_contr = contr_bystatus_index.upper_bound( indexby_value );

    // stakingtoken.cpp - L:88
    // check( row->payout_date.sec_since_epoch() <
    //            eosio::current_time_point().sec_since_epoch(),
    //        "invalid date, not yet ready to claim" );
    for ( ; curr_contr != end_contr && curr_contr->stake_date < end_slot_time &&
            limit > 0;
          --limit ) {
      eosio::action( eosio::permission_level{ get_self(), "active"_n },
                     SUPPORTED_STAKING_CONTRACT,
                     "claim"_n,
                     std::make_tuple( get_self(), curr_contr->stake_index ) )
          .send();

      contr_bystatus_index.modify( curr_contr, get_self(), [&]( auto &row ) {
        row.status = e_stake_status::CLAIMED;
      } );

      curr_contr = contr_bystatus_index.lower_bound( indexby_value );
      end_contr = contr_bystatus_index.upper_bound( indexby_value );
    }

    auto process = stakeprocess_sing.get();

    if ( curr_contr == end_contr || curr_contr->stake_date >= end_slot_time ) {
      process = stakeprocess_standby{
          .next_contribution_date = stakeprocess.contribution_date +
                                    eosio::hours( CONTRIBUTION_TIME_HOUR_SLOT ),
          .next_stake_index = stakeprocess.next_stake_index,
          .next_index_cache = stakeprocess.next_index_cache };
    }

    stakeprocess_sing.set( process, get_self() );
  }

  void mining::apply_halving() {
    auto global = global_sing.get();

    global.current_allocation_per_day /= 2;

    if ( global.current_allocation_per_day < MINIMUM_LIBRE_ALLOCATION ) {
      global.current_allocation_per_day = MINIMUM_LIBRE_ALLOCATION;
    }

    global.next_halving_date += eosio::days( HALVING_STEP );

    global_sing.set( global, get_self() );
  }

  void mining::issue_and_transfer_tokens( eosio::asset total_tokens ) {
    action( permission_level{ TOKEN_ISSUER, eosio::name( "stake" ) },
            eosio::name( SUPPORTED_TOKEN_LIBRE_CONTRACT ),
            eosio::name( "issue" ),
            std::make_tuple( TOKEN_ISSUER,
                             total_tokens,
                             "tokens issued for " + get_self().to_string() ) )
        .send();

    action(
        permission_level{ TOKEN_ISSUER, eosio::name( "stake" ) },
        eosio::name( SUPPORTED_TOKEN_LIBRE_CONTRACT ),
        eosio::name( "transfer" ),
        std::make_tuple( TOKEN_ISSUER,
                         get_self(),
                         total_tokens,
                         "tokens transferred for " + get_self().to_string() ) )
        .send();
  }

  eosio::asset mining::calculate_allocation_reward(
      const eosio::asset &pbtc_contributed,
      const eosio::asset &total_pbtc_contributed,
      const eosio::asset &total_allocation_per_day ) {
    double contr_percent = static_cast< double >( pbtc_contributed.amount ) /
                           total_pbtc_contributed.amount;

    return eosio::asset( contr_percent * total_allocation_per_day.amount,
                         SUPPORTED_TOKEN_LIBRE_SYMBOL );
  }

  void mining::send_leftofver_allocation(
      const eosio::asset          &amount,
      const eosio::time_point_sec &contribution_date ) {
    add_balance( SUPPORTED_DAO_CONTRACT, amount );

    std::string leftover_funds_memo =
        DAO_FOUNDING_TRANSFER_MEMO + ": leftover funds";

    eosio::action( eosio::permission_level{ get_self(), "active"_n },
                   SUPPORTED_TOKEN_LIBRE_CONTRACT,
                   "transfer"_n,
                   std::make_tuple( get_self(),
                                    SUPPORTED_DAO_CONTRACT,
                                    amount,
                                    leftover_funds_memo ) )
        .send();
  }

  void mining::send_daily_pbtc_contribution(
      const eosio::asset          &amount,
      const eosio::time_point_sec &contribution_date ) {
    add_balance( SUPPORTED_DAO_CONTRACT, amount );

    std::string pbtc_contribution_memo =
        DAO_FOUNDING_TRANSFER_MEMO + ": " +
        SUPPORTED_TOKEN_BTC_SYMBOL.code().to_string() + " " +
        get_self().to_string() + " contribution on " +
        std::to_string( contribution_date.sec_since_epoch() );

    eosio::action( eosio::permission_level{ get_self(), "active"_n },
                   SUPPORTED_TOKEN_BTC_CONTRACT,
                   "transfer"_n,
                   std::make_tuple( get_self(),
                                    SUPPORTED_DAO_CONTRACT,
                                    amount,
                                    pbtc_contribution_memo ) )
        .send();
  }

  void mining::add_balance( const eosio::name  &account,
                            const eosio::asset &quantity ) {
    distribution_table_type distribution_tb( get_self(),
                                             quantity.symbol.code().raw() );
    auto distribution_itr = distribution_tb.find( account.value );

    if ( distribution_itr == distribution_tb.end() ) {
      distribution_tb.emplace( get_self(), [&]( auto &row ) {
        row.account = account;
        row.reward_amount = quantity;
        row.total_claimed = quantity;
      } );
    } else {
      distribution_tb.modify( distribution_itr, get_self(), [&]( auto &row ) {
        row.reward_amount += quantity;
        row.total_claimed += quantity;
      } );
    }
  }

  void mining::sub_balance( const eosio::name  &account,
                            const eosio::asset &quantity ) {
    distribution_table_type distribution_tb( get_self(),
                                             quantity.symbol.code().raw() );
    auto distribution_itr = distribution_tb.find( account.value );

    eosio::check( distribution_itr != distribution_tb.end(),
                  "account does not have a balance" );
    eosio::check( distribution_itr->reward_amount >= quantity,
                  "overdrawn balance" );

    distribution_tb.modify( distribution_itr, get_self(), [&]( auto &row ) {
      row.reward_amount -= quantity;
    } );
  }

} // namespace mining

EOSIO_ACTION_DISPATCHER( mining::actions )
EOSIO_ABIGEN( actions( mining::actions ),
              table( "global"_n, mining::global ),
              table( "contribution"_n, mining::contribution ),
              table( "stakeprocess"_n, mining::stakeprocess_variant ),
              table( "distribution"_n, mining::distribution ) )
#include <tester-base.hpp>

#define CATCH_CONFIG_RUNNER

#ifdef RUN_TESTS

TEST_CASE( "init mining contract" ) {
  tester t;

  t.fund_accounts();
  t.setup_staking_contract( "2023-10-01T00:00:00.000" );

  auto start_date = t.get_block_time_sec() + eosio::days( 1 );

  expect( t.alice.trace< mining::actions::init >(
              INITIAL_ALLOCATION,
              eosio::time_point_sec{ 1696118400 },
              TEN_YEARS_IN_DAYS ),
          "missing authority of mining.libre" );
  expect( t.mining.trace< mining::actions::init >(
              s2a( "131071999.0000 LIBRE" ),
              eosio::time_point_sec{ 1696118400 },
              TEN_YEARS_IN_DAYS ),
          "assertion failure with message: expected initial allocation to be "
          "2000000.0000 LIBRE" );
  expect( t.mining.trace< mining::actions::init >(
              INITIAL_ALLOCATION,
              eosio::time_point_sec{ 1696118400 },
              3649 ),
          "assertion failure with message: expected total days: 3600" );

  expect( t.mining.trace< mining::actions::init >( INITIAL_ALLOCATION,
                                                   t.get_block_time_sec() -
                                                       eosio::days( 1 ),
                                                   TEN_YEARS_IN_DAYS ),
          "assertion failure with message: start date must be in the future" );

  t.mining.act< mining::actions::init >( INITIAL_ALLOCATION,
                                         start_date,
                                         TEN_YEARS_IN_DAYS );

  auto global =
      t.get_singleton< mining::global_singleton_type, mining::global >();
  mining::global expected_global = {
      .start_date = start_date,
      .end_date = eosio::time_point_sec( start_date +
                                         eosio::days( TEN_YEARS_IN_DAYS ) ),
      .current_allocation_per_day = INITIAL_ALLOCATION,
      .initial_libre_allocation = INITIAL_ALLOCATION,
      .total_amount_allocated = s2a( "0.0000 LIBRE" ),
      .next_halving_date =
          eosio::time_point_sec( start_date + eosio::days( 180 ) ) };

  CHECK( global.start_date == expected_global.start_date );
  CHECK( global.end_date == expected_global.end_date );
  CHECK( global.next_halving_date == expected_global.next_halving_date );
  CHECK( global.current_allocation_per_day ==
         expected_global.current_allocation_per_day );
  CHECK( global.initial_libre_allocation ==
         expected_global.initial_libre_allocation );
  CHECK( global.total_amount_allocated ==
         expected_global.total_amount_allocated );
}

TEST_CASE( "add contribution" ) {
  tester t;

  t.fund_accounts();
  t.setup_staking_contract( "2023-10-01T00:00:00.000" );

  auto start_date = t.get_block_time_sec() + eosio::days( 1 );

  expect( t.alice.with_code( "btc.libre"_n )
              .trace< token::actions::transfer >( "alice"_n,
                                                  "mining.libre"_n,
                                                  s2a( "1.000000000 FAKE" ),
                                                  "contribute on day 1" ),
          "assertion failure with message: invalid symbol, expected: BTC" );
  expect( t.alice.with_code( "btc.libre"_n )
              .trace< token::actions::transfer >( "alice"_n,
                                                  "mining.libre"_n,
                                                  s2a( "0.10000000 BTC" ),
                                                  "contribute on day 1" ),
          "assertion failure with message: contract not initialized" );

  t.init_contract( start_date );

  expect( t.alice.with_code( "btc.libre"_n )
              .trace< token::actions::transfer >( "alice"_n,
                                                  "mining.libre"_n,
                                                  s2a( "0.10000000 BTC" ),
                                                  "contribute on day 1" ),
          "assertion failure with message: contribution period not open yet" );

  t.skip_to( "2023-10-02T00:00:00.000" );
  t.alice.with_code( "btc.libre"_n )
      .act< token::actions::transfer >( "alice"_n,
                                        "mining.libre"_n,
                                        s2a( "0.10000000 BTC" ),
                                        "contribute on day 1" );

  mining::contribution expected_contribution = {
      .index = 0,
      .account = "alice"_n,
      .btc_amount = s2a( "0.10000000 BTC" ),
      .contr_date = eosio::time_point_sec{ 1696118400 + 1 * 86400 },
      .stake_index = 0,
      .stake_amount = s2a( "0.0000 LIBRE" ),
      .stake_date = eosio::time_point_sec{},
      .status = mining::e_stake_status::PENDING };

  auto contribution = t.get_contribution( 0 );

  CHECK( contribution.index == expected_contribution.index );
  CHECK( contribution.account == expected_contribution.account );
  CHECK( contribution.btc_amount == expected_contribution.btc_amount );
  CHECK( contribution.contr_date == expected_contribution.contr_date );
  CHECK( contribution.stake_index == expected_contribution.stake_index );
  CHECK( contribution.stake_amount == expected_contribution.stake_amount );
  CHECK( contribution.stake_date == expected_contribution.stake_date );
  CHECK( contribution.status == expected_contribution.status );

  t.skip_to( "2023-11-29T23:59:59.500" );
  t.alice.with_code( "btc.libre"_n )
      .act< token::actions::transfer >( "alice"_n,
                                        "mining.libre"_n,
                                        s2a( "0.10000000 BTC" ),
                                        "contribute on day 1" );

  CHECK( t.get_contribution( 1 ).contr_date != eosio::time_point_sec{} );

  t.skip_to( "2033-10-02T00:00:00.000" );

  expect( t.alice.with_code( "btc.libre"_n )
              .trace< token::actions::transfer >( "alice"_n,
                                                  "mining.libre"_n,
                                                  s2a( "0.10000000 BTC" ),
                                                  "contribute on day 1" ),
          "contribution period has ended" );
}

TEST_CASE( "default stake process" ) {
  tester t;

  t.fund_accounts();
  t.setup_staking_contract( "2023-10-01T00:00:00.000" );

  // IMPORTANT NOTE: this step is only for unit tests, it is done inside of the smart contract and need to be tested manually
  t.issue_and_transfer_required_tokens();

  auto start_date = t.get_block_time_sec() + eosio::days( 1 );

  expect( t.alice.trace< mining::actions::process >( 100 ),
          "contract not initialized" );

  t.init_contract( start_date );
  t.skip_to( "2023-10-02T00:00:00.000" );
  t.add_contributions( 1 );

  CHECK( t.get_table_size< mining::contribution_table_type >() == 3 );

  expect( t.alice.trace< mining::actions::process >( 1 ),
          "not ready to process" );

  t.skip_to( "2023-10-03T00:00:00.000" );

  t.alice.act< mining::actions::process >( 1 );

  auto stakeprocess = t.get_singleton< mining::stakeprocess_singleton_type,
                                       mining::stakeprocess_variant >();
  CHECK( std::holds_alternative< mining::stakeprocess_count >( stakeprocess ) );

  t.alice.act< mining::actions::process >( 2 );

  stakeprocess = t.get_singleton< mining::stakeprocess_singleton_type,
                                  mining::stakeprocess_variant >();
  auto *process_allocate =
      std::get_if< mining::stakeprocess_allocate >( &stakeprocess );

  CHECK( process_allocate != nullptr );
  CHECK( process_allocate->contribution_date ==
         eosio::time_point_sec( start_date + eosio::days( 1 ) ) );
  CHECK( process_allocate->total_BTC_contributed ==
         s2a( "0.30000000 BTC" ) );
  CHECK( process_allocate->current_allocation == INITIAL_ALLOCATION );

  t.chain.start_block();
  t.alice.act< mining::actions::process >( 2 );
  t.chain.start_block();
  t.alice.act< mining::actions::process >( 1 );

  CHECK( t.get_table_size< libre::stake_table >( "stake.libre"_n ) == 3 );

  stakeprocess = t.get_singleton< mining::stakeprocess_singleton_type,
                                  mining::stakeprocess_variant >();
  auto *process_link =
      std::get_if< mining::stakeprocess_link >( &stakeprocess );

  CHECK( process_link != nullptr );
  CHECK( t.get_token_balance( "dao.libre"_n,
                              "btc.libre"_n,
                              eosio::symbol_code{ "BTC" } ) ==
         s2a( "0.30000000 BTC" ) );

  t.alice.act< mining::actions::process >( 2 );
  t.chain.start_block();
  t.alice.act< mining::actions::process >( 1 );
  t.chain.start_block();

  stakeprocess = t.get_singleton< mining::stakeprocess_singleton_type,
                                  mining::stakeprocess_variant >();

  // internally moved to stakeprocess_claim but contract detects it is not time yet
  // to claim stakes so it moves to stakeprocess_standby right away

  auto *process_standby =
      std::get_if< mining::stakeprocess_standby >( &stakeprocess );

  CHECK( process_standby != nullptr );

  auto contr_alice = t.get_table_object< mining::contribution_table_type,
                                         mining::contribution >( 0 );
  auto contr_bob = t.get_table_object< mining::contribution_table_type,
                                       mining::contribution >( 1 );
  auto contr_pip = t.get_table_object< mining::contribution_table_type,
                                       mining::contribution >( 2 );

  auto stake_alice =
      t.get_table_object< libre::stake_table, libre::stake >( 0,
                                                              "stake.libre"_n );
  auto stake_bob =
      t.get_table_object< libre::stake_table, libre::stake >( 1,
                                                              "stake.libre"_n );
  auto stake_pip =
      t.get_table_object< libre::stake_table, libre::stake >( 2,
                                                              "stake.libre"_n );

  CHECK( contr_alice.stake_index == stake_alice.index );
  CHECK( contr_bob.stake_index == stake_bob.index );
  CHECK( contr_pip.stake_index == stake_pip.index );

  CHECK( contr_alice.stake_date == stake_alice.stake_date );
  CHECK( contr_bob.stake_date == stake_bob.stake_date );
  CHECK( contr_pip.stake_date == stake_pip.stake_date );

  CHECK( contr_alice.stake_payout == stake_alice.payout );
  CHECK( contr_bob.stake_payout == stake_bob.payout );
  CHECK( contr_pip.stake_payout == stake_pip.payout );

  CHECK( contr_alice.status == mining::e_stake_status::STAKE_LINKED );
  CHECK( contr_bob.status == mining::e_stake_status::STAKE_LINKED );
  CHECK( contr_pip.status == mining::e_stake_status::STAKE_LINKED );

  expect( t.alice.trace< mining::actions::process >( 1 ),
          "not ready to process" );

  t.skip_to( "2023-10-04T00:00:00.000" );
  t.alice.act< mining::actions::process >( 1 );
  t.chain.start_block();
  t.alice.act< mining::actions::process >( 100 );
  t.skip_to( "2024-03-31T00:00:01.000" );

  expect( t.alice.trace< mining::actions::withdraw >( 0 ),
          "contribution is not ready to be claimed" );

  t.alice.act< mining::actions::process >( 100 );
  t.chain.start_block();
  t.alice.act< mining::actions::process >( 1 );
  t.chain.start_block();
  t.alice.act< mining::actions::process >( 1 );

  expect( t.alice.trace< mining::actions::withdraw >( 3 ),
          "contribution not found" );

  stakeprocess = t.get_singleton< mining::stakeprocess_singleton_type,
                                  mining::stakeprocess_variant >();
  auto *process_claim =
      std::get_if< mining::stakeprocess_claim >( &stakeprocess );

  CHECK( process_claim != nullptr );

  // NOTE: removed "stake" required permission from stake.libre contract for testing purposes
  t.chain.start_block();
  t.alice.act< mining::actions::process >( 5 );

  stakeprocess = t.get_singleton< mining::stakeprocess_singleton_type,
                                  mining::stakeprocess_variant >();
  process_standby =
      std::get_if< mining::stakeprocess_standby >( &stakeprocess );

  CHECK( process_standby != nullptr );

  contr_alice = t.get_table_object< mining::contribution_table_type,
                                    mining::contribution >( 0 );
  contr_bob = t.get_table_object< mining::contribution_table_type,
                                  mining::contribution >( 1 );
  contr_pip = t.get_table_object< mining::contribution_table_type,
                                  mining::contribution >( 2 );

  stake_alice =
      t.get_table_object< libre::stake_table, libre::stake >( 0,
                                                              "stake.libre"_n );
  stake_bob =
      t.get_table_object< libre::stake_table, libre::stake >( 1,
                                                              "stake.libre"_n );
  stake_pip =
      t.get_table_object< libre::stake_table, libre::stake >( 2,
                                                              "stake.libre"_n );

  CHECK( contr_alice.status == mining::e_stake_status::CLAIMED );
  CHECK( contr_bob.status == mining::e_stake_status::CLAIMED );
  CHECK( contr_pip.status == mining::e_stake_status::CLAIMED );

  CHECK( stake_alice.status == libre::stake_status::STAKE_COMPLETED );
  CHECK( stake_bob.status == libre::stake_status::STAKE_COMPLETED );
  CHECK( stake_pip.status == libre::stake_status::STAKE_COMPLETED );

  t.skip_to( "2024-04-01T00:00:00.000" );

  t.alice.act< mining::actions::process >( 50 );

  expect( t.alice.trace< mining::actions::process >( 100 ),
          "nothing to process" );
  expect( t.bob.trace< mining::actions::withdraw >( 0 ),
          "missing authority of alice" );

  t.alice.act< mining::actions::withdraw >( 0 );
  t.bob.act< mining::actions::withdraw >( 1 );
  t.pip.act< mining::actions::withdraw >( 2 );

  CHECK( contr_alice.stake_payout == stake_alice.payout );
  CHECK( t.get_token_balance( "alice"_n ) == contr_alice.stake_payout );

  CHECK( contr_bob.stake_payout == stake_bob.payout );
  CHECK( t.get_token_balance( "bob"_n ) == contr_bob.stake_payout );

  CHECK( contr_pip.stake_payout == stake_pip.payout );
  CHECK( t.get_token_balance( "pip"_n ) == contr_pip.stake_payout );
}

TEST_CASE( "validate allocation amounts" ) {
  tester t;

  t.fund_accounts();
  t.setup_staking_contract( "2023-10-01T00:00:00.000" );

  // IMPORTANT NOTE: this step is only for unit tests, it is done inside of the smart contract and need to be tested manually
  t.issue_and_transfer_required_tokens();

  auto start_date = t.get_block_time_sec() + eosio::days( 1 );

  t.init_contract( start_date );
  t.skip_to( "2023-10-02T00:00:00.000" );
  t.alice.with_code( "btc.libre"_n )
      .act< token::actions::transfer >( "alice"_n,
                                        "mining.libre"_n,
                                        s2a( "0.10000000 BTC" ),
                                        "add contribute" );
  t.skip_to( "2023-10-03T00:00:00.000" );
  t.alice.act< mining::actions::process >( 100 );
  t.chain.start_block();
  t.alice.act< mining::actions::process >( 100 );

  CHECK( t.get_table_size< mining::contribution_table_type >() == 1 );
  CHECK( t.get_table_size< libre::stake_table >( "stake.libre"_n ) == 1 );

  auto contr_alice = t.get_table_object< mining::contribution_table_type,
                                         mining::contribution >( 0 );
  auto stake_alice =
      t.get_table_object< libre::stake_table, libre::stake >( 0,
                                                              "stake.libre"_n );

  CHECK( contr_alice.stake_amount == INITIAL_ALLOCATION );
  CHECK( stake_alice.libre_staked == INITIAL_ALLOCATION );

  t.add_contributions( 1 );
  t.skip_to( "2023-10-04T00:00:00.000" );
  t.alice.act< mining::actions::process >( 100 );
  t.chain.start_block();
  t.alice.act< mining::actions::process >( 100 );

  contr_alice = t.get_table_object< mining::contribution_table_type,
                                    mining::contribution >( 1 );
  auto contr_bob = t.get_table_object< mining::contribution_table_type,
                                       mining::contribution >( 2 );
  auto contr_pip = t.get_table_object< mining::contribution_table_type,
                                       mining::contribution >( 3 );

  stake_alice =
      t.get_table_object< libre::stake_table, libre::stake >( 1,
                                                              "stake.libre"_n );
  auto stake_bob =
      t.get_table_object< libre::stake_table, libre::stake >( 2,
                                                              "stake.libre"_n );
  auto stake_pip =
      t.get_table_object< libre::stake_table, libre::stake >( 3,
                                                              "stake.libre"_n );

  CHECK( contr_alice.stake_amount == s2a( "666666.6666 LIBRE" ) );
  CHECK( contr_bob.stake_amount == s2a( "666666.6666 LIBRE" ) );
  CHECK( contr_pip.stake_amount == s2a( "666666.6666 LIBRE" ) );

  CHECK( stake_alice.libre_staked == s2a( "666666.6666 LIBRE" ) );
  CHECK( stake_bob.libre_staked == s2a( "666666.6666 LIBRE" ) );
  CHECK( stake_pip.libre_staked == s2a( "666666.6666 LIBRE" ) );

  t.alice.with_code( "btc.libre"_n )
      .act< token::actions::transfer >( "alice"_n,
                                        "mining.libre"_n,
                                        s2a( "1.00000000 BTC" ),
                                        "add contribute" );
  t.bob.with_code( "btc.libre"_n )
      .act< token::actions::transfer >( "bob"_n,
                                        "mining.libre"_n,
                                        s2a( "1.00000000 BTC" ),
                                        "add contribute" );

  t.skip_to( "2023-10-05T00:00:00.000" );
  t.alice.act< mining::actions::process >( 100 );
  t.chain.start_block();
  t.alice.act< mining::actions::process >( 100 );

  contr_alice = t.get_table_object< mining::contribution_table_type,
                                    mining::contribution >( 4 );
  contr_bob = t.get_table_object< mining::contribution_table_type,
                                  mining::contribution >( 5 );

  stake_alice =
      t.get_table_object< libre::stake_table, libre::stake >( 4,
                                                              "stake.libre"_n );
  stake_bob =
      t.get_table_object< libre::stake_table, libre::stake >( 5,
                                                              "stake.libre"_n );

  CHECK( contr_alice.stake_amount == s2a( "1000000.0000 LIBRE" ) );
  CHECK( contr_bob.stake_amount == s2a( "1000000.0000 LIBRE" ) );

  CHECK( stake_alice.libre_staked == s2a( "1000000.0000 LIBRE" ) );
  CHECK( stake_bob.libre_staked == s2a( "1000000.0000 LIBRE" ) );

  // no contributions in a day
  t.skip_to( "2023-10-06T00:00:00.000" );
  t.alice.act< mining::actions::process >( 100 );
  t.chain.start_block();
  t.alice.act< mining::actions::process >( 100 );

  // allocation is sent to the dao.libre contract
  CHECK(
      t.get_token_balance( "dao.libre"_n ) ==
      s2a(
          "2000000.0002 LIBRE" ) ); // 2000000.0002 LIBRE = (1000000.0000 LIBRE - 333333.3333 LIBRE * 3) + 1000000.0000 LIBRE

  t.skip_to( "2023-10-07T00:00:00.000" );
  t.alice.act< mining::actions::process >( 100 );
  t.chain.start_block();
  t.alice.act< mining::actions::process >( 100 );

  CHECK(
      t.get_token_balance( "dao.libre"_n ) ==
      s2a(
          "4000000.0002 LIBRE" ) ); // 4000000.0002 LIBRE = 2000000.0002 LIBRE + 2000000.0000 LIBRE

  t.alice.with_code( "btc.libre"_n )
      .act< token::actions::transfer >( "alice"_n,
                                        "mining.libre"_n,
                                        s2a( "0.10000000 BTC" ),
                                        "add contribute" );
  t.bob.with_code( "btc.libre"_n )
      .act< token::actions::transfer >( "bob"_n,
                                        "mining.libre"_n,
                                        s2a( "0.20000000 BTC" ),
                                        "add contribute" );
  t.pip.with_code( "btc.libre"_n )
      .act< token::actions::transfer >( "pip"_n,
                                        "mining.libre"_n,
                                        s2a( "0.30000000 BTC" ),
                                        "add contribute" );
  t.egeon.with_code( "btc.libre"_n )
      .act< token::actions::transfer >( "egeon"_n,
                                        "mining.libre"_n,
                                        s2a( "0.40000000 BTC" ),
                                        "add contribute" );
  t.bertie.with_code( "btc.libre"_n )
      .act< token::actions::transfer >( "bertie"_n,
                                        "mining.libre"_n,
                                        s2a( "0.50000000 BTC" ),
                                        "add contribute" );
  t.skip_to( "2023-10-08T00:00:00.000" );
  t.alice.act< mining::actions::process >( 100 );
  t.chain.start_block();
  t.alice.act< mining::actions::process >( 100 );

  contr_alice = t.get_table_object< mining::contribution_table_type,
                                    mining::contribution >( 6 );
  contr_bob = t.get_table_object< mining::contribution_table_type,
                                  mining::contribution >( 7 );
  contr_pip = t.get_table_object< mining::contribution_table_type,
                                  mining::contribution >( 8 );
  auto contr_egeon = t.get_table_object< mining::contribution_table_type,
                                         mining::contribution >( 9 );
  auto contr_bertie = t.get_table_object< mining::contribution_table_type,
                                          mining::contribution >( 10 );

  stake_alice =
      t.get_table_object< libre::stake_table, libre::stake >( 6,
                                                              "stake.libre"_n );
  stake_bob =
      t.get_table_object< libre::stake_table, libre::stake >( 7,
                                                              "stake.libre"_n );
  stake_pip =
      t.get_table_object< libre::stake_table, libre::stake >( 8,
                                                              "stake.libre"_n );
  auto stake_egeon =
      t.get_table_object< libre::stake_table, libre::stake >( 9,
                                                              "stake.libre"_n );
  auto stake_bertie =
      t.get_table_object< libre::stake_table, libre::stake >( 10,
                                                              "stake.libre"_n );

  CHECK( contr_alice.stake_amount == s2a( "133333.3333 LIBRE" ) );
  CHECK( contr_bob.stake_amount == s2a( "266666.6666 LIBRE" ) );
  CHECK( contr_pip.stake_amount == s2a( "400000.0000 LIBRE" ) );
  CHECK( contr_egeon.stake_amount == s2a( "533333.3333 LIBRE" ) );
  CHECK( contr_bertie.stake_amount == s2a( "666666.6666 LIBRE" ) );

  CHECK( stake_alice.libre_staked == s2a( "133333.3333 LIBRE" ) );
  CHECK( stake_bob.libre_staked == s2a( "266666.6666 LIBRE" ) );
  CHECK( stake_pip.libre_staked == s2a( "400000.0000 LIBRE" ) );
  CHECK( stake_egeon.libre_staked == s2a( "533333.3333 LIBRE" ) );
  CHECK( stake_bertie.libre_staked == s2a( "666666.6666 LIBRE" ) );
}

TEST_CASE( "allocation link with already existing stakes" ) {
  tester t;

  t.fund_accounts();
  t.eosio_token.act< token::actions::transfer >( "eosio.token"_n,
                                                 "alice"_n,
                                                 s2a( "100.0000 LIBRE" ),
                                                 "funding" );
  t.eosio_token.act< token::actions::transfer >( "eosio.token"_n,
                                                 "bob"_n,
                                                 s2a( "100.0000 LIBRE" ),
                                                 "funding" );
  t.eosio_token.act< token::actions::transfer >( "eosio.token"_n,
                                                 "pip"_n,
                                                 s2a( "100.0000 LIBRE" ),
                                                 "funding" );

  t.setup_staking_contract( "2023-10-01T00:00:00.000" );

  // IMPORTANT NOTE: this step is only for unit tests, it is done inside of the smart contract and need to be tested manually
  t.issue_and_transfer_required_tokens();

  t.init_contract( eosio::time_point_sec{ 1696118400 + 1 * 86400 } );
  t.skip_to( "2023-10-02T00:00:00.000" );
  t.alice.act< token::actions::transfer >( "alice"_n,
                                           "stake.libre"_n,
                                           s2a( "10.0000 LIBRE" ),
                                           "stakefor:120" );
  t.bob.act< token::actions::transfer >( "bob"_n,
                                         "stake.libre"_n,
                                         s2a( "10.0000 LIBRE" ),
                                         "stakefor:120" );
  t.pip.act< token::actions::transfer >( "pip"_n,
                                         "stake.libre"_n,
                                         s2a( "10.0000 LIBRE" ),
                                         "stakefor:120" );
  t.add_contributions( 1 );
  t.skip_to( "2023-10-03T00:00:00.000" );
  t.alice.act< mining::actions::process >( 100 );
  t.chain.start_block();
  t.alice.act< mining::actions::process >( 100 );

  auto contr_alice = t.get_table_object< mining::contribution_table_type,
                                         mining::contribution >( 0 );
  auto contr_bob = t.get_table_object< mining::contribution_table_type,
                                       mining::contribution >( 1 );
  auto contr_pip = t.get_table_object< mining::contribution_table_type,
                                       mining::contribution >( 2 );

  CHECK( contr_alice.stake_index == 3 );
  CHECK( contr_bob.stake_index == 4 );
  CHECK( contr_pip.stake_index == 5 );

  t.alice.act< token::actions::transfer >( "alice"_n,
                                           "stake.libre"_n,
                                           s2a( "10.0000 LIBRE" ),
                                           "stakefor:120" );
  t.bob.act< token::actions::transfer >( "bob"_n,
                                         "stake.libre"_n,
                                         s2a( "10.0000 LIBRE" ),
                                         "stakefor:120" );
  t.pip.act< token::actions::transfer >( "pip"_n,
                                         "stake.libre"_n,
                                         s2a( "10.0000 LIBRE" ),
                                         "stakefor:120" );
  t.chain.start_block();
  t.pip.act< token::actions::transfer >( "pip"_n,
                                         "stake.libre"_n,
                                         s2a( "10.0000 LIBRE" ),
                                         "stakefor:120" );
  t.chain.start_block();
  t.pip.act< token::actions::transfer >( "pip"_n,
                                         "stake.libre"_n,
                                         s2a( "10.0000 LIBRE" ),
                                         "stakefor:120" );

  t.chain.start_block();
  t.add_contributions( 1 );
  t.chain.start_block();
  t.pip.act< token::actions::transfer >( "pip"_n,
                                         "stake.libre"_n,
                                         s2a( "10.0000 LIBRE" ),
                                         "stakefor:120" );
  t.skip_to( "2023-10-04T00:00:00.000" );
  t.alice.act< mining::actions::process >( 100 );
  t.chain.start_block();
  t.alice.act< mining::actions::process >( 100 );

  contr_alice = t.get_table_object< mining::contribution_table_type,
                                    mining::contribution >( 3 );
  contr_bob = t.get_table_object< mining::contribution_table_type,
                                  mining::contribution >( 4 );
  contr_pip = t.get_table_object< mining::contribution_table_type,
                                  mining::contribution >( 5 );

  CHECK( contr_alice.stake_index == 12 );
  CHECK( contr_bob.stake_index == 13 );
  CHECK( contr_pip.stake_index == 14 );

  t.chain.start_block();
  t.add_contributions( 1 );
  t.chain.start_block();
  t.pip.act< token::actions::transfer >( "pip"_n,
                                         "stake.libre"_n,
                                         s2a( "10.0000 LIBRE" ),
                                         "stakefor:120" );
  t.skip_to( "2023-10-05T00:00:00.000" );
  t.alice.act< mining::actions::process >( 100 );
  t.chain.start_block();
  t.alice.act< mining::actions::process >( 200 );

  contr_alice = t.get_table_object< mining::contribution_table_type,
                                    mining::contribution >( 6 );
  contr_bob = t.get_table_object< mining::contribution_table_type,
                                  mining::contribution >( 7 );
  contr_pip = t.get_table_object< mining::contribution_table_type,
                                  mining::contribution >( 8 );

  CHECK( contr_alice.stake_index == 16 );
  CHECK( contr_bob.stake_index == 17 );
  CHECK( contr_pip.stake_index == 18 );

  auto  stakeprocess = t.get_singleton< mining::stakeprocess_singleton_type,
                                        mining::stakeprocess_variant >();
  auto *process_standby =
      std::get_if< mining::stakeprocess_standby >( &stakeprocess );

  CHECK( process_standby != nullptr );
  CHECK( process_standby->next_index_cache == 9 );
  CHECK( process_standby->next_stake_index == 19 );
}

TEST_CASE( "full contribution - even" ) {
  tester t;

  t.fund_accounts();
  t.setup_staking_contract( "2023-10-01T00:00:00.000" );

  // IMPORTANT NOTE: this step is only for unit tests, it is done inside of the smart contract and need to be tested manually
  t.issue_and_transfer_required_tokens();

  auto start_date =
      eosio::time_point_sec( t.get_block_time_sec() + eosio::days( 1 ) );
  auto end_date =
      eosio::time_point_sec( start_date + eosio::days( TEN_YEARS_IN_DAYS ) );

  t.init_contract( start_date );
  t.skip_to( "2023-10-02T00:00:00.000" );

  t.complete_contributions( { "alice"_n, "bob"_n, "pip"_n, "egeon"_n } );

  expect( t.alice.with_code( "btc.libre"_n )
              .trace< token::actions::transfer >( "alice"_n,
                                                  "mining.libre"_n,
                                                  s2a( "0.10000000 BTC" ),
                                                  "add contribute on day " ),
          "contribution period has ended" );

  auto global =
      t.get_singleton< mining::global_singleton_type, mining::global >();

  CHECK( global.start_date == start_date );
  CHECK( global.end_date == end_date );
  CHECK( global.current_allocation_per_day == s2a( "100.0000 LIBRE" ) );
  CHECK( global.initial_libre_allocation == INITIAL_ALLOCATION );
  CHECK( global.total_amount_allocated == s2a( "720068027.1840 LIBRE" ) );

  // total contributions: 3600 days of contributions period * 4 contributions (alice, bob, pip, egeon)
  // per day = 14400
  t.skip_to( "2035-10-02T00:00:00.000" );

  t.alice.act< mining::actions::process >( 100 );
  t.alice.act< mining::actions::process >( 20000 );

  std::vector< eosio::name > contributors = { "alice"_n,
                                              "bob"_n,
                                              "pip"_n,
                                              "egeon"_n };

  for ( uint16_t contr_index = 0; contr_index < 14400; ) {
    for ( auto account : contributors ) {
      t.chain.as( account ).act< mining::actions::withdraw >( contr_index );
      ++contr_index;
      t.chain.start_block();
    }
  }
}

TEST_CASE( "full contribution - odd" ) {
  tester t;

  t.fund_accounts();
  t.setup_staking_contract( "2023-10-01T00:00:00.000" );

  // IMPORTANT NOTE: this step is only for unit tests, it is done inside of the smart contract and need to be tested manually
  t.issue_and_transfer_required_tokens();

  auto start_date =
      eosio::time_point_sec( t.get_block_time_sec() + eosio::days( 1 ) );
  auto end_date =
      eosio::time_point_sec( start_date + eosio::days( TEN_YEARS_IN_DAYS ) );

  t.init_contract( start_date );
  t.skip_to( "2023-10-02T00:00:00.000" );
  t.complete_contributions();

  expect( t.alice.with_code( "btc.libre"_n )
              .trace< token::actions::transfer >( "alice"_n,
                                                  "mining.libre"_n,
                                                  s2a( "0.10000000 BTC" ),
                                                  "add contribute on day " ),
          "contribution period has ended" );

  auto global =
      t.get_singleton< mining::global_singleton_type, mining::global >();

  CHECK( global.start_date == start_date );
  CHECK( global.end_date == end_date );
  CHECK( global.current_allocation_per_day == s2a( "100.0000 LIBRE" ) );
  CHECK( global.initial_libre_allocation == INITIAL_ALLOCATION );
  CHECK( global.total_amount_allocated == s2a( "720068026.9140 LIBRE" ) );
}

TEST_CASE( "frozen contribution" ) {
  tester t;

  t.fund_accounts();
  t.btc_ptokens.with_code( "btc.libre"_n )
      .act< token::actions::transfer >( "btc.libre"_n,
                                        "alice"_n,
                                        s2a( "1000.00000000 BTC" ),
                                        "fund account" );
  t.setup_staking_contract( "2023-10-01T00:00:00.000" );

  // IMPORTANT NOTE: this step is only for unit tests, it is done inside of the smart contract and need to be tested manually
  t.issue_and_transfer_required_tokens();

  auto start_date =
      eosio::time_point_sec( t.get_block_time_sec() + eosio::days( 1 ) );

  t.init_contract( start_date );
  t.skip_to( "2023-10-02T00:00:00.000" );

  // fund the account with enough BTC to force the FROZEN state
  t.btc_ptokens.with_code( "btc.libre"_n )
      .act< token::actions::transfer >( "btc.libre"_n,
                                        "alice"_n,
                                        s2a( "90000000.00000000 BTC" ),
                                        "fund account" );

  t.alice.with_code( "btc.libre"_n )
      .act< token::actions::transfer >( "alice"_n,
                                        MINING_CONTRACT_NAME,
                                        s2a( "90000000.00000000 BTC" ),
                                        "add contribute on day" );
  t.bob.with_code( "btc.libre"_n )
      .act< token::actions::transfer >( "bob"_n,
                                        MINING_CONTRACT_NAME,
                                        s2a( "0.000000001 BTC" ),
                                        "add contribute on day" );
  t.skip_to( "2023-10-03T00:00:00.000" );
  t.alice.act< mining::actions::process >( 100 );
  t.chain.start_block();
  t.alice.act< mining::actions::process >( 200 );

  auto contribution = t.get_contribution( 0 );
  auto contribution_frozen = t.get_contribution( 1 );

  CHECK( contribution.status == mining::e_stake_status::STAKE_LINKED );
  CHECK( contribution_frozen.status == mining::e_stake_status::FROZEN );
}

TEST_CASE( "full halving" ) {
  tester t;

  t.fund_accounts();
  t.setup_staking_contract( "2023-10-01T00:00:00.000" );

  // IMPORTANT NOTE: this step is only for unit tests, it is done inside of the smart contract and need to be tested manually
  t.issue_and_transfer_required_tokens();

  auto start_date =
      eosio::time_point_sec( t.get_block_time_sec() + eosio::days( 1 ) );
  auto end_date =
      eosio::time_point_sec( start_date + eosio::days( TEN_YEARS_IN_DAYS ) );

  expect( t.alice.trace< mining::actions::process >( 100 ),
          "contract not initialized" );

  t.init_contract( start_date );

  auto global =
      t.get_singleton< mining::global_singleton_type, mining::global >();

  CHECK( global.current_allocation_per_day == INITIAL_ALLOCATION );
  CHECK( global.total_amount_allocated == s2a( "0.0000 LIBRE" ) );
  CHECK( global.next_halving_date ==
         eosio::time_point_sec( start_date + eosio::days( 180 ) ) );

  t.skip_to( "2023-10-02T00:00:00.000" );

  uint8_t      total_halving = 20;
  uint16_t     current_day_step = 180;
  uint16_t     days_range = 180;
  eosio::asset temp_allocation = INITIAL_ALLOCATION;
  eosio::asset temp_total_allocation = s2a( "0.0000 LIBRE" );

  for ( size_t i = 0; i < total_halving; i++ ) {
    temp_total_allocation += temp_allocation * 180;

    t.add_contributions( days_range, { "alice"_n } );
    t.process_for( days_range );

    // expect( t.alice.trace< mining::actions::process >( 1 ),
    //         "not ready to process" );

    global = t.get_singleton< mining::global_singleton_type, mining::global >();

    CHECK( global.current_allocation_per_day == temp_allocation );
    CHECK( global.total_amount_allocated == temp_total_allocation );
    CHECK(
        global.next_halving_date ==
        eosio::time_point_sec( start_date + eosio::days( current_day_step ) ) );

    temp_allocation /= 2;

    if ( temp_allocation < MINIMUM_LIBRE_ALLOCATION ) {
      temp_allocation = MINIMUM_LIBRE_ALLOCATION;
    }

    current_day_step += 180;

    t.chain.start_block();
  }

  global = t.get_singleton< mining::global_singleton_type, mining::global >();

  CHECK( global.current_allocation_per_day == MINIMUM_LIBRE_ALLOCATION );
  CHECK( global.total_amount_allocated == s2a( "720068027.3280 LIBRE" ) );
  CHECK( global.next_halving_date == end_date );
}

#endif
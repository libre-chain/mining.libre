#pragma once

#include <eosio/tester.hpp>

#include <mining.hpp>
#include <stakingtoken.hpp>
#include <token/token.hpp>

// Catch2 unit testing framework. https://github.com/catchorg/Catch2
#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

using namespace eosio;
using user_context = test_chain::user_context;

eosio::name  MINING_CONTRACT_NAME{ "mining.libre"_n };
eosio::asset INITIAL_ALLOCATION = s2a( "2000000.0000 LIBRE" );
eosio::asset MINIMUM_LIBRE_ALLOCATION = s2a( "100.0000 LIBRE" );
uint16_t     TEN_YEARS_IN_DAYS = 3600;

void mining_setup( test_chain &t ) {
  t.create_code_account( MINING_CONTRACT_NAME );
  t.set_code( MINING_CONTRACT_NAME, "mining.wasm" );
}

void dao_setup( test_chain &t ) {
  t.create_code_account( "dao.libre"_n );
  // t.set_code( "dao.libre"_n, "dao.wasm" );
}

void staking_setup( test_chain &t ) {
  t.create_code_account( "stake.libre"_n );
  t.set_code( "stake.libre"_n, "stakingtoken.wasm" );
}

void token_setup( test_chain &t ) {
  t.create_code_account( "eosio.token"_n );
  t.create_code_account( "usdt.ptokens"_n );
  t.create_code_account( "btc.ptokens"_n );

  t.set_code( "eosio.token"_n, "token.wasm" );
  t.set_code( "usdt.ptokens"_n, "token.wasm" );
  t.set_code( "btc.ptokens"_n, "token.wasm" );

  t.as( "eosio.token"_n )
      .act< token::actions::create >( "eosio.token"_n,
                                      s2a( "100000000000000.0000 LIBRE" ) );
  t.as( "eosio.token"_n )
      .act< token::actions::issue >( "eosio.token"_n,
                                     s2a( "100000000000000.0000 LIBRE" ),
                                     "" );

  t.as( "usdt.ptokens"_n )
      .with_code( "usdt.ptokens"_n )
      .act< token::actions::create >( "usdt.ptokens"_n,
                                      s2a( "1000000000.000000000 PUSDT" ) );
  t.as( "usdt.ptokens"_n )
      .with_code( "usdt.ptokens"_n )
      .act< token::actions::issue >( "usdt.ptokens"_n,
                                     s2a( "231902.183285165 PUSDT" ),
                                     "" );

  t.as( "btc.ptokens"_n )
      .with_code( "btc.ptokens"_n )
      .act< token::actions::create >( "btc.ptokens"_n,
                                      s2a( "210000000.000000000 PBTC" ) );
  t.as( "btc.ptokens"_n )
      .with_code( "btc.ptokens"_n )
      .act< token::actions::issue >( "btc.ptokens"_n,
                                     s2a( "210000000.000000000 PBTC" ),
                                     "" );

  t.as( "btc.ptokens"_n )
      .with_code( "btc.ptokens"_n )
      .act< token::actions::create >( "btc.ptokens"_n,
                                      s2a( "21000000.000000000 FAKE" ) );
  t.as( "btc.ptokens"_n )
      .with_code( "btc.ptokens"_n )
      .act< token::actions::issue >( "btc.ptokens"_n,
                                     s2a( "21000000.000000000 FAKE" ),
                                     "" );

  t.as( "eosio.token"_n )
      .act< token::actions::create >( "eosio.token"_n,
                                      s2a( "10000000000.0000 OTHER" ) );
  t.as( "eosio.token"_n )
      .act< token::actions::issue >( "eosio.token"_n,
                                     s2a( "10000000000.0000 OTHER" ),
                                     "" );
}

struct tester {
  test_chain   chain;
  user_context mining = chain.as( MINING_CONTRACT_NAME );
  user_context stakelibre = chain.as( "stake.libre"_n );
  user_context eosio_token = chain.as( "eosio.token"_n );
  user_context btc_ptokens = chain.as( "btc.ptokens"_n );

  user_context alice = chain.as( "alice"_n );
  user_context bob = chain.as( "bob"_n );
  user_context pip = chain.as( "pip"_n );
  user_context egeon = chain.as( "egeon"_n );
  user_context bertie = chain.as( "bertie"_n );
  user_context ahab = chain.as( "ahab"_n );

  std::vector< eosio::name > test_accounts =
      { "alice"_n, "bob"_n, "pip"_n, "egeon"_n, "bertie"_n, "ahab"_n };

  tester() {
    mining_setup( chain );
    dao_setup( chain );
    staking_setup( chain );
    token_setup( chain );

    for ( auto account : test_accounts ) {
      chain.create_account( account );
    }
  }

  eosio::time_point_sec get_block_time_sec() {
    return eosio::time_point_sec{ chain.get_head_block_info()
                                      .timestamp.to_time_point()
                                      .sec_since_epoch() };
  }

  void skip_to( std::string_view time ) {
    uint64_t value;
    check( string_to_utc_microseconds( value,
                                       time.data(),
                                       time.data() + time.size() ),
           "bad time" );

    time_point tp{ microseconds( value ) };

    chain.finish_block();
    auto head_tp = chain.get_head_block_info().timestamp.to_time_point();
    auto skip = ( tp - head_tp ).count() / 1000 - 500;
    chain.start_block( skip );
  }

  void fund_accounts() {
    for ( auto account : test_accounts ) {
      eosio_token.act< token::actions::transfer >( "eosio.token"_n,
                                                   account,
                                                   s2a( "1000000.0000 OTHER" ),
                                                   "fund account" );

      btc_ptokens.with_code( "btc.ptokens"_n )
          .act< token::actions::transfer >( "btc.ptokens"_n,
                                            account,
                                            s2a( "10000.000000000 PBTC" ),
                                            "fund account" );

      btc_ptokens.with_code( "btc.ptokens"_n )
          .act< token::actions::transfer >( "btc.ptokens"_n,
                                            account,
                                            s2a( "100.000000000 FAKE" ),
                                            "fund account with fake tokens" );
    }

    eosio_token.act< token::actions::transfer >(
        "eosio.token"_n,
        "stake.libre"_n,
        s2a( "100000000000.0000 LIBRE" ),
        "funding:3650" );
  }

  void setup_staking_contract( std::string_view &&date ) {
    skip_to( date );

    stakelibre.act< libre::actions::init >( eosio::time_point_sec{ 1696118400 },
                                            365 );
  }

  template < typename T > void init_contract( T &&start_time ) {
    mining.act< mining::actions::init >( INITIAL_ALLOCATION,
                                         start_time,
                                         TEN_YEARS_IN_DAYS );
  }

  void add_contributions( uint8_t                    days,
                          std::vector< eosio::name > contributors =
                              { "alice"_n, "bob"_n, "pip"_n } ) {
    auto skip_day = 86400 * 1000;

    for ( uint8_t day = 0; day < days; ++day ) {
      for ( auto account : contributors ) {
        chain.as( account )
            .with_code( "btc.ptokens"_n )
            .act< token::actions::transfer >( account,
                                              MINING_CONTRACT_NAME,
                                              s2a( "0.100000000 PBTC" ),
                                              "add contribute on day " );
      }

      if ( days > 1 ) {
        chain.start_block( skip_day );
      }
    }
  }

  void process_for( uint8_t days ) {
    for ( uint8_t day = 0; day < days; ++day ) {
      alice.act< mining::actions::process >( 5000 );
      chain.start_block();
      bob.act< mining::actions::process >( 5000 );

      if ( days > 1 ) {
        chain.start_block();
      }
    }
  }

  void issue_and_transfer_required_tokens() {
    // NOTE: all tokens are already issued
    // eosio_token.act< token::actions::issue >( "eosio.token"_n,
    //                                           total_tokens,
    //                                           "issue with stake permission" );

    eosio::asset required_balance = s2a( "0.0000 LIBRE" );
    eosio::asset current_allocation_amount = INITIAL_ALLOCATION;

    for ( size_t day = 0; day <= TEN_YEARS_IN_DAYS; ++day ) {
      if ( day % 365 == 0 ) {
        current_allocation_amount /= 2;

        if ( current_allocation_amount < MINIMUM_LIBRE_ALLOCATION ) {
          current_allocation_amount = MINIMUM_LIBRE_ALLOCATION;
        }
      }

      required_balance += current_allocation_amount;
    }

    eosio_token.act< token::actions::transfer >(
        "eosio.token"_n,
        MINING_CONTRACT_NAME,
        required_balance,
        "transfer with stake permission" );
  }

  void complete_contributions( std::vector< eosio::name > contributors =
                                   { "alice"_n, "bob"_n, "pip"_n } ) {
    auto skip_day = 86400 * 1000;

    for ( uint16_t i = 0; i < TEN_YEARS_IN_DAYS; ++i ) {
      add_contributions( 1, contributors );
      chain.start_block( skip_day );
      alice.act< mining::actions::process >( 100 );
      chain.start_block();
      alice.act< mining::actions::process >( 100 );
    }
  }

  template < typename SingletonType, typename ReturnType >
  ReturnType get_singleton( eosio::name code = MINING_CONTRACT_NAME ) {
    SingletonType sing{ code, code.value };

    return sing.get();
  }

  template < typename TableType, typename ObjectType >
  ObjectType get_table_object( uint64_t    index,
                               eosio::name code = MINING_CONTRACT_NAME ) {
    TableType table{ code, code.value };

    auto itr = table.find( index );

    return itr != table.end() ? *itr : ObjectType{};
  }

  auto get_token_balance( eosio::name owner,
                          eosio::name token_contract_account = "eosio.token"_n,
                          eosio::symbol_code sym_code = eosio::symbol_code{
                              "LIBRE" } ) {
    return token::contract::get_balance( token_contract_account,
                                         owner,
                                         sym_code );
  }

  mining::contribution get_contribution( uint64_t index ) {
    mining::contribution_table_type contribution_tb{
        MINING_CONTRACT_NAME,
        MINING_CONTRACT_NAME.value };

    auto itr = contribution_tb.find( index );

    return itr != contribution_tb.end() ? *itr : mining::contribution{};
  }

  template < typename T >
  auto get_table_size( eosio::name code = MINING_CONTRACT_NAME ) {
    T tb( code, code.value );
    return std::distance( tb.begin(), tb.end() );
  }
};